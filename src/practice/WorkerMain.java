package practice;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.ChronoUnit;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class WorkerMain {
    public static void main(String[] args) {

        List<Worker> workers = WorkerUtils.getWorkerList();
        Predicate<Double> salCalculate = sal -> (sal < 60000);
        Function<LocalDate, Integer> ageCalculation = dob -> (Period.between(dob,LocalDate.now()).getYears());

        //1.List of Workers whose salary is less then 60000
        List<String> list = workers.stream()
                .filter(w -> salCalculate.test(w.getSal()))
                .map(w -> w.getName())
                .collect(Collectors.toList());

        System.out.println(list.size());

        //2.Count of workers whose salary greater than 30000 less than 60000
        //3.Oldest worker,get worker object and print
        Worker oldestWorker = workers.stream()
                .min(Comparator.comparing(worker -> worker.getDob()))
                .get();
        System.out.println(oldestWorker);
        //4.Junior worker,get worker object and print
        //5.Highest paid worker,get worker object and print
        //6.lowest paid worker,get worker object and print
        //7.Sort worker age, lowest to Highest,get list of worker names and print
        //8.Sort worker age , highest to lowest,get list of worker names and print
        //9.Average Salary of all workers
        //10.Average age of all workers
        double avgAge = workers.stream()
                .mapToInt(w -> ageCalculation.apply(w.getDob()))
                .average()
                .getAsDouble();

        System.out.println(avgAge);
        //11.Group Using Department and print for each department
        //12.count of worker age> 20 and salary <30000
        //13.Worker Count from each group
        //14.Total Salary of Each dept
        //15.Average salary of each dept
        //16.Oldest employee of each dept
        //17.Given the list of worker, find the worker with name “Some Name”.
        //18.Join the all Worker names with “,” using java 8
        //19.remove the duplicate elements from the list
        //20.List of employee whose name starts with "A"
    }
}
